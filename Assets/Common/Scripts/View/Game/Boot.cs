using NovaPlay.Scripts.View;
using UnityEngine;

public class Boot : MonoBehaviour
{
    public static Game Game;
    
    private void Awake()
    {
        Game = new Game();
    }
}
